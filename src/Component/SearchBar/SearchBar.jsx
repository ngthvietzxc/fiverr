import React from "react";

export default function SearchBar() {
  return (
    <div>
      <form className="p-4">
        <div className="flex">
          <div className="relative w-full">
            <input
              style={{ width: 580, height: 40 }}
              type="search"
              id="search-dropdown"
              className="block z-20 text-md font-medium text-gray-900 bg-gray-50 rounded border-2-gray-50  border border-gray-300 focus:ring-black focus:border-black dark:bg-gray-700 dark:border-l-gray-700  dark:border-gray-600 dark:placeholder-gray dark:text-white dark:focus:border-black"
              placeholder="What service are you looking for today?"
              required
            />
            <button
              style={{ height: 40, width: 45, textAlign: "center" }}
              type="submit"
              className="absolute top-0 right-0 p-2.5 text-sm font-medium text-white bg-black rounded border border-black hover:bg-black focus:ring-4 focus:outline-none focus:ring-black dark:bg-black dark:hover:bg-black dark:focus:ring-black"
            >
              <svg
                aria-hidden="true"
                className="w-5 h-5"
                fill="none"
                stroke="currentColor"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                />
              </svg>
              <span className="sr-only">Search</span>
            </button>
          </div>
        </div>
      </form>
    </div>
  );
}
