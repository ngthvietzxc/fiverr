import React, { useEffect } from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import { useDispatch, useSelector } from "react-redux";
import { addBanner } from "./../../redux/action/layoutAction";

export default function Banner() {
  const settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
  };
  const dispatch = useDispatch();
  const banner = useSelector((state) => state.layoutReducer.banners);

  useEffect(() => {
    dispatch(addBanner());
  }, []);

  return (
    <div>
      <Slider {...settings}>
        {banner.map((banner, index) => (
          <img
            key={index}
            src={banner.imageUrl}
            style={{ width: "100%", objectFit: "cover" }}
            className="object-top"
          />
        ))}
      </Slider>
    </div>
  );
}
