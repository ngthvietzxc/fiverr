import React from "react";

export default function () {
  return (
    <div className="flex font-bold whitespace-nowrap">
      <nav>
        <ul className="flex gap-3">
          <li>
            <a className="inline-block px-4 py-2 text-white cursor-pointer">
              Fiverr Business
            </a>
            <a className="inline-block px-4 py-2 text-white cursor-pointer">
              Explore
            </a>
            <a className="inline-block px-4 py-2 text-white cursor-pointer">
              Become a Seller
            </a>
          </li>
          <li>
            <a className="inline-block px-4 py-2 text-white cursor-pointer">
              Sign in
            </a>
          </li>
          <li className=" border-2 rounded border-white hover:bg-green-500 hover:border-green-500">
            <a className=" inline-block px-4 py-2 text-white cursor-pointer">
              Join
            </a>
          </li>
        </ul>
      </nav>
    </div>
  );
}
