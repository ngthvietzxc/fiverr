import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSquareCheck } from "@fortawesome/free-solid-svg-icons";

export default function SellingProposition() {
  return (
    <div style={{ backgroundColor: "#f1fdf7" }} className="pt-24 pb-24">
      <div className="container flex mx-auto grid grid-cols-2 ">
        <div>
          <h2
            style={{
              fontSize: 32,
              paddingBottom: 24,
              lineHeight: "120%",
              fontWeight: 700,
            }}
            className="flex"
          >
            The best part? Everything.
          </h2>
          <div className="pb-6">
            <div className="flex pb-2" style={{ color: "#404145" }}>
              <FontAwesomeIcon className="w-6 h-6" icon={faSquareCheck} />
              <p
                className="ml-6"
                style={{ lineHeight: "140%", fontWeight: 700, fontSize: 18 }}
              >
                Stick to your budget
              </p>
            </div>
            <p className="flex">
              Find the right service for every price point. No hourly rates,
              just project-based pricing.
            </p>
          </div>
          <div className="pb-6">
            <div className="flex pb-2" style={{ color: "#404145" }}>
              <FontAwesomeIcon className="w-6 h-6" icon={faSquareCheck} />
              <p
                className="ml-6"
                style={{ lineHeight: "140%", fontWeight: 700, fontSize: 18 }}
              >
                Get quality work done quickly
              </p>
            </div>
            <p className="flex">
              Hand your project over to a talented freelancer in minutes, get
              long-lasting results.
            </p>
          </div>
          <div className="pb-6">
            <div className="flex pb-2" style={{ color: "#404145" }}>
              <FontAwesomeIcon className="w-6 h-6" icon={faSquareCheck} />
              <p
                className="ml-6"
                style={{ lineHeight: "140%", fontWeight: 700, fontSize: 18 }}
              >
                Pay when you're happy
              </p>
            </div>
            <p className="flex">
              Upfront quotes mean no surprises. Payments only get released when
              you approve.
            </p>
          </div>
          <div className="pb-6">
            <div className="flex pb-2" style={{ color: "#404145" }}>
              <FontAwesomeIcon className="w-6 h-6" icon={faSquareCheck} />
              <p
                className="ml-6"
                style={{ lineHeight: "140%", fontWeight: 700, fontSize: 18 }}
              >
                Count on 24/7 support
              </p>
            </div>
            <p className="flex">
              Our round-the-clock support team is available to help anytime,
              anywhere.
            </p>
          </div>
        </div>
        <div>
          <video
            width={670}
            height={430}
            className="orca-video"
            controls
            poster="https://fiverr-res.cloudinary.com/q_auto,f_auto,w_700,dpr_1.0/v1/attachments/generic_asset/asset/089e3bb9352f90802ad07ad9f6a4a450-1599517407052/selling-proposition-still-1400-x1.png"
            preload="metadata"
            crossOrigin="anonymous"
          >
            <source
              src="https://fiverr-res.cloudinary.com/video/upload/t_fiverr_hd/vmvv3czyk2ifedefkau7"
              type="video/mp4"
            />
          </video>
        </div>
      </div>
    </div>
  );
}
