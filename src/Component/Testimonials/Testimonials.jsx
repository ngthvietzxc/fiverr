import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { addTestimonials } from "../../redux/action/layoutAction";
import Slider from "react-slick";

export default function Testimonials() {
  const settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };
  const dispatch = useDispatch();
  const testimonial = useSelector((state) => state.layoutReducer.testimonials);

  useEffect(() => {
    dispatch(addTestimonials());
  }, []);
  return (
    <div className="pt-24 pb-24">
      <Slider {...settings}>
        {testimonial.map((testimonial, index) => (
          <div className="container mx-auto">
            <div className="flex items-center">
              <div className="w-2/5">
                <video
                  key={index}
                  width={560}
                  height={320}
                  className="orca-video"
                  controls
                  poster={testimonial.imgVideo}
                  preload="metadata"
                  crossOrigin="anonymous"
                >
                  <source src={testimonial.video} type="video/mp4" />
                </video>
              </div>
              <div className="w-3/5">
                <div className="flex items-center pb-4" style={{ height: 36 }}>
                  <div
                    className="text-xl pr-5 border-r"
                    style={{ color: "#74767e" }}
                  >
                    {testimonial.name}
                  </div>
                  <div>
                    <img
                      src={testimonial.logo}
                      style={{
                        height: 36,
                        marginLeft: 12,
                        verticalAlign: "middle",
                        borderLeft: 1,
                      }}
                      alt=""
                    />
                  </div>
                </div>
                <div
                  className="italic text-left"
                  style={{
                    fontWeight: 500,
                    fontSize: 30,
                    color: "#003912",
                  }}
                >
                  {testimonial.quote}
                </div>
              </div>
            </div>
          </div>
        ))}
      </Slider>
    </div>
  );
}
