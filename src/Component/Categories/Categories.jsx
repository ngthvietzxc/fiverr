import React from "react";

export default function Categories() {
  return (
    <div style={{ backgroundColor: "#f5f5f5" }}>
      <div className="container mx-auto pb-24 pt-24">
        <h2
          className="pb-6 mb-10"
          style={{
            fontSize: 32,

            lineHeight: "120%",
            fontWeight: 700,
            display: "flex",
          }}
        >
          You need it, we've got it
        </h2>
        <ul className="categories-list flex flex-wrap grid grid-cols-5  justify-center">
          <li className="block pb-14 px-2.5">
            <a
              className="text-center"
              href="/categories/graphics-design?source=hplo_cat_sec&pos=1"
              data-uw-rm-brl="false"
            >
              <img
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/graphics-design.91dfe44.svg"
                alt="Graphics & Design"
                className="w-12 h-12 mx-auto mb-4 hover:animate-spin "
              />
              Graphics &amp; Design
            </a>
          </li>
          <li className="block pb-14 px-2.5">
            <a href="/categories/online-marketing?source=hplo_cat_sec&pos=2">
              <img
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/online-marketing.a3e9794.svg"
                alt="Digital Marketing"
                className="w-12 h-12 mx-auto mb-4 hover:animate-spin"
              />
              Digital Marketing
            </a>
          </li>
          <li className="block pb-14 px-2.5">
            <a href="/categories/writing-translation?source=hplo_cat_sec&pos=3">
              <img
                className="w-12 h-12 mx-auto mb-4 hover:animate-spin"
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/writing-translation.a787f2f.svg"
                alt="Writing & Translation"
              />
              Writing &amp; Translation
            </a>
          </li>
          <li className="block pb-14 px-2.5">
            <a href="/categories/video-animation?source=hplo_cat_sec&pos=4">
              <img
                className="w-12 h-12 mx-auto mb-4 hover:animate-spin"
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/video-animation.1356999.svg"
                alt="Video & Animation"
              />
              Video &amp; Animation
            </a>
          </li>
          <li className="block pb-14 px-2.5">
            <a href="/categories/music-audio?source=hplo_cat_sec&pos=5">
              <img
                className="w-12 h-12 mx-auto mb-4 hover:animate-spin"
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/music-audio.ede4c90.svg"
                alt="Music & Audio"
              />
              Music &amp; Audio
            </a>
          </li>
          <li className="block pb-14 px-2.5">
            <a href="/categories/programming-tech?source=hplo_cat_sec&pos=6">
              <img
                className="w-12 h-12 mx-auto mb-4 hover:animate-spin"
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/programming.6ee5a90.svg"
                alt="Programming & Tech"
              />
              Programming &amp; Tech
            </a>
          </li>
          <li className="block pb-14 px-2.5">
            <a href="/categories/business?source=hplo_cat_sec&pos=7">
              <img
                className="w-12 h-12 mx-auto mb-4 hover:animate-spin"
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/business.fabc3a7.svg"
                alt="Business"
              />
              Business
            </a>
          </li>
          <li className="block pb-14 px-2.5">
            <a href="/categories/lifestyle?source=hplo_cat_sec&pos=8">
              <img
                className="w-12 h-12 mx-auto mb-4 hover:animate-spin"
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/lifestyle.112b348.svg"
                alt="Lifestyle"
              />
              Lifestyle
            </a>
          </li>
          <li className="block pb-14 px-2.5">
            <a href="/categories/data?source=hplo_cat_sec&pos=9">
              <img
                className="w-12 h-12 mx-auto mb-4 hover:animate-spin"
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/data.855fe95.svg"
                alt="Data"
              />
              Data
            </a>
          </li>
          <li className="block pb-14 px-2.5">
            <a href="/categories/photography?source=hplo_cat_sec&pos=10">
              <img
                className="w-12 h-12 mx-auto mb-4 hover:animate-spin"
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/photography.0cf5a3f.svg"
                alt="Photography"
              />
              Photography
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
}
