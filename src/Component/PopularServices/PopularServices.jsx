import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Card } from "antd";
import Slider from "react-slick";

import { addServiceList } from "../../redux/action/layoutAction";

const { Meta } = Card;

export default function PopularServices() {
  const settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    autoplay: true,
    dots: true,
  };
  const dispatch = useDispatch();
  const popularServices = useSelector(
    (state) => state.layoutReducer.popularServices
  );
  useEffect(() => {
    dispatch(addServiceList());
  }, []);
  return (
    <div className="container mx-auto pb-20">
      <h2
        style={{
          fontSize: 32,
          paddingBottom: 24,
          lineHeight: "120%",
          fontWeight: 700,
        }}
        className="flex"
      >
        Popular Services
      </h2>
      <Slider {...settings}>
        {popularServices.map((services, index) => (
          <div>
            <Card
              key={index}
              className="m-5"
              hoverable
              cover={<img src={services.hinhAnh} />}
            >
              <Meta title={services.tieuDe} description={services.moTa} />
            </Card>
          </div>
        ))}
      </Slider>
    </div>
  );
}
