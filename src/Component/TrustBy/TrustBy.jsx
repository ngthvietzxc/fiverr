import React from "react";

export default function TrustBy() {
  return (
    <div style={{ paddingBottom: 45 }}>
      <div
        className="flex trusted-by max-width-container items-center  justify-center"
        style={{ backgroundColor: "#fafafa", height: 95 }}
      >
        <span
          style={{
            display: "block",
            color: "#b5b6ba",
            fontWeight: 600,
            paddingRight: 20,
            fontSize: 16,
          }}
        >
          Trusted by:
        </span>
        <ul className="flex items-center gap-10">
          <li className>
            <picture>
              <source
                media="(max-width: 899px)"
                srcSet="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/meta.99a0dda.png 1x, https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/meta2x.d8d5bc3.png 2x"
              />
              <source
                media="(min-width: 900px)"
                srcSet="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/meta.12b5e5c.png 1x, https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/meta2x.b364aec.png 2x"
              />
              <img
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/meta.12b5e5c.png"
                alt="facebook"
                data-uw-rm-ima-original="facebook"
              />
            </picture>
          </li>
          <li className>
            <picture>
              <source
                media="(max-width: 899px)"
                srcSet="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/google.9d71a37.png 1x, https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/google2x.0053d08.png 2x"
              />
              <source
                media="(min-width: 900px)"
                srcSet="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/google.61e78c8.png 1x, https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/google2x.4fa6c20.png 2x"
              />
              <img
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/google.61e78c8.png"
                alt="Google"
                data-uw-rm-ima-original="google"
              />
            </picture>
          </li>
          <li className>
            <picture>
              <source
                media="(max-width: 899px)"
                srcSet="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/netflix.35dc5cd.png 1x, https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/netflix2x.9022712.png 2x"
              />
              <source
                media="(min-width: 900px)"
                srcSet="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/netflix.96c5e3f.png 1x, https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/netflix2x.6b36ad6.png 2x"
              />
              <img
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/netflix.96c5e3f.png"
                alt="NETFLIX"
                data-uw-rm-ima-original="netflix"
              />
            </picture>
          </li>
          <li className>
            <picture>
              <source
                media="(max-width: 899px)"
                srcSet="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/pandg.967b1ad.png 1x, https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/pandg2x.6665fbe.png 2x"
              />
              <source
                media="(min-width: 900px)"
                srcSet="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/pandg.0f4cfc2.png 1x, https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/pandg2x.0d06f7b.png 2x"
              />
              <img
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/pandg.0f4cfc2.png"
                alt="P&G"
                data-uw-rm-ima-original="p&g"
              />
            </picture>
          </li>
          <li className="display-from-sm">
            <picture>
              <source
                media="(max-width: 899px)"
                srcSet="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/paypal.910e738.png 1x, https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/paypal2x.bd199ac.png 2x"
              />
              <source
                media="(min-width: 900px)"
                srcSet="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/paypal.305e264.png 1x, https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/paypal2x.d2fa54d.png 2x"
              />
              <img
                src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/paypal.305e264.png"
                alt="PayPal"
                data-uw-rm-ima-original="paypal"
              />
            </picture>
          </li>
        </ul>
      </div>
    </div>
  );
}
