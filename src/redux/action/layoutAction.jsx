import {
  ADD_BANNER,
  ADD_SERVICE_LIST,
  ADD_TESTIMONIALS,
} from "../constant/layoutConstant";

export const addBanner = (banner) => ({
  type: ADD_BANNER,
  payload: banner,
});

export const addServiceList = (services) => {
  return {
    type: ADD_SERVICE_LIST,
    payload: services,
  };
};
export const addTestimonials = (testimonials) => {
  return {
    type: ADD_TESTIMONIALS,
    payload: testimonials,
  };
};
