import {
  ADD_BANNER,
  ADD_SERVICE_LIST,
  ADD_TESTIMONIALS,
} from "./../constant/layoutConstant";

const initialState = {
  banners: [
    {
      title: "Banner 1",
      imageUrl:
        "https://fiverr-res.cloudinary.com/image/upload/f_auto,q_auto/v1/attachments/generic_asset/asset/1b6990afe0934244dda2c9aeed5de8d9-1674663021930/bg-hero-6-1792-x1.png",
    },
    {
      title: "Banner 2",
      imageUrl:
        "https://fiverr-res.cloudinary.com/image/upload/q_auto,f_auto/v1/attachments/generic_asset/asset/bb5958e41c91bb37f4afe2a318b71599-1599344049983/bg-hero-1-1792-x1.png",
    },
    {
      title: "Banner 3",
      imageUrl:
        "https://fiverr-res.cloudinary.com/image/upload/q_auto,f_auto/v1/attachments/generic_asset/asset/2413b8415dda9dbd7756d02cb87cd4b1-1599595203045/bg-hero-2-1792-x1.png",
    },
    {
      title: "Banner 4",
      imageUrl:
        "https://fiverr-res.cloudinary.com/image/upload/q_auto,f_auto/v1/attachments/generic_asset/asset/d14871e2d118f46db2c18ad882619ea8-1599835783966/bg-hero-3-1792-x1.png",
    },
    {
      title: "Banner 5",
      imageUrl:
        "https://fiverr-res.cloudinary.com/image/upload/q_auto,f_auto/v1/attachments/generic_asset/asset/93085acc959671e9e9e77f3ca8147f82-1599427734108/bg-hero-4-1792-x1.png",
    },
    {
      title: "Banner 6",
      imageUrl:
        "https://fiverr-res.cloudinary.com/image/upload/q_auto,f_auto/v1/attachments/generic_asset/asset/bb5958e41c91bb37f4afe2a318b71599-1599344049970/bg-hero-5-1792-x1.png",
    },
  ],
  popularServices: [
    {
      tieuDe: "Al Artists",
      moTa: "Add talend to Al",
      hinhAnh:
        "https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/f27bec553efc12cc60baed89b8f2223e-1674661140708/ai-artists-2x.png",
    },
    {
      tieuDe: "Logo Design",
      moTa: "Build your brand",
      hinhAnh:
        "https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/logo-design-2x.png",
    },
    {
      tieuDe: "WordPress",
      moTa: "Customize your site",
      hinhAnh:
        "https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/ae11e2d45410b0eded7fba0e46b09dbd-1598561917003/wordpress-2x.png",
    },
    {
      tieuDe: "Voice Over",
      moTa: "Share your message",
      hinhAnh:
        "https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741669/voiceover-2x.png",
    },
    {
      tieuDe: "Video Explainer",
      moTa: "Engage your audience",
      hinhAnh:
        "https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741663/animated-explainer-2x.png",
    },
    {
      tieuDe: "Social Media",
      moTa: "Reach more customers",
      hinhAnh:
        "https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741667/social-2x.png",
    },
    {
      tieuDe: "SEO",
      moTa: "Unlock growth online",
      hinhAnh:
        "https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741668/seo-2x.png",
    },
    {
      tieuDe: "Illustration",
      moTa: "Color your dreams",
      hinhAnh:
        "https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/illustration-2x.png",
    },
    {
      tieuDe: "Translation",
      moTa: "Go global",
      hinhAnh:
        "https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741674/translation-2x.png",
    },
    {
      tieuDe: "Data Entry",
      moTa: "Learn your business",
      hinhAnh:
        "https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741664/data-entry-2x.png",
    },
    {
      tieuDe: "Showcase your story",
      moTa: "Book Covers",
      hinhAnh:
        "https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741678/book-covers-2x.png",
    },
  ],
  testimonials: [
    {
      video:
        "https://fiverr-res.cloudinary.com/video/upload/t_fiverr_hd/bsncmkwya3nectkensun",
      imgVideo:
        "https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173395/testimonial-video-still-haerfest.jpg",
      name: "Tim and Dan Joo, Co-Founders",
      logo: "https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/haerfest-logo-x2.934ab63.png",
      quote:
        '"When you want to create a business bigger than yourself, you need a lot of help. That\'s what Fiverr does."',
    },
    {
      video: "",
      imgVideo:
        "https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173414/testimonial-video-still-naadam.jpg",
      name: "Caitlin Tormey, Chief Commercial Officer",
      logo: "https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/naadam-logo-x2.a79031d.png",
      quote:
        '"We\'ve used Fiverr for Shopify web development, graphic design, and backend web development. Working with Fiverr makes my job a little easier every day."',
    },
    {
      video: "",
      imgVideo:
        "https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173396/testimonial-video-still-lavender.jpg",
      name: "Brighid Gannon (DNP, PMHNP-BC), Co-Founder",
      logo: "https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/lavender-logo-x2.3fff9e7.png",
      quote:
        '"We used Fiverr for SEO, our logo, website, copy, animated videos — literally everything. It was like working with a human right next to you versus being across the world."',
    },
    {
      video: "",
      imgVideo:
        "https://fiverr-res.cloudinary.com/q_auto,f_auto,w_560,dpr_1.0/v1/attachments/generic_asset/asset/42a6fd208670a0361b38bd72b47b9317-1599519173399/testimonial-video-still-rooted.jpg",
      name: "Kay Kim, Co-Founder",
      logo: "https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/rooted-logo-x2.7da3bc9.png",
      quote:
        "\"It's extremely exciting that Fiverr has freelancers from all over the world — it broadens the talent pool. One of the best things about Fiverr is that while we're sleeping, someone's working.\"",
    },
  ],
};

export default function layoutReducer(state = initialState, action) {
  switch (action.type) {
    case ADD_BANNER:
      return {
        ...state,
        banner: [...state.banners, action.payload],
      };
    case ADD_SERVICE_LIST:
      return {
        ...state,
        popularService: [...state.popularServices, action.payload],
      };
    case ADD_TESTIMONIALS:
      return {
        ...state,
        testimonial: [...state.testimonials, action.payload],
      };
    default:
      return state;
  }
}
