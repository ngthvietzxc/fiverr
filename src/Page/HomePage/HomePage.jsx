import React from "react";
import Banner from "../../Component/Banner/Banner";
import Header from "../../Component/Header/Header";
import TrustBy from "../../Component/TrustBy/TrustBy";
import PopularServices from "../../Component/PopularServices/PopularServices";
import Footer from "../../Component/Footer/Footer";
import SellingProposition from "../../Component/SellingProposition/SellingProposition";
import Categories from "../../Component/Categories/Categories";
import Testimonials from "../../Component/Testimonials/Testimonials";

export default function HomePage() {
  return (
    <div>
      <Header />
      <Banner />
      <TrustBy />
      <PopularServices />
      <SellingProposition />
      <Testimonials />
      <Categories />
      <Footer />
    </div>
  );
}
