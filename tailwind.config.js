/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
    "./node_modules/flowbite/**/*.js",
    "./node_modules/flowbite-react/**/*.{js,jsx,ts,tsx}", // thêm đường dẫn cho Flowbite React
  ],
  theme: {
    extend: {},
  },
  plugins: [
    require("flowbite/plugin"), // thêm plugin Flowbite
  ],
};
